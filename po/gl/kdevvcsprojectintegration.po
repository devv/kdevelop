# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Xosé <xosecalvo@gmail.com>, 2012.
# Marce Villarino <mvillarino@kde-espana.org>, 2013.
# Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2017-07-28 11:43+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: vcschangesview.cpp:84
#, fuzzy, kde-format
#| msgid "Refresh"
msgctxt "@action:inmenu"
msgid "Refresh"
msgstr "Anovar"

#: vcschangesviewplugin.cpp:62
#, fuzzy, kde-format
#| msgid "Project Changes"
msgctxt "@title:window"
msgid "Project Changes"
msgstr "Cambios do proxecto"

#: vcschangesviewplugin.cpp:65
#, fuzzy, kde-format
#| msgid "Locate Current Document"
msgctxt "@action"
msgid "Locate Current Document"
msgstr "Atopar o documento actual"

#: vcschangesviewplugin.cpp:67
#, fuzzy, kde-format
#| msgid "Locates the current document and selects it."
msgctxt "@info:tooltip"
msgid "Locate the current document and select it"
msgstr "Atopa o documento actual e selecciónao."

#: vcschangesviewplugin.cpp:70
#, fuzzy, kde-format
#| msgid "Reload View"
msgctxt "@action"
msgid "Reload View"
msgstr "Cargar de novo a vista"

#: vcschangesviewplugin.cpp:72
#, fuzzy, kde-format
#| msgid "Refreshes the view for all projects, in case anything changed."
msgctxt "@info:tooltip"
msgid "Refresh the view for all projects, in case anything changed"
msgstr "Anova a vista de todos os proxectos, por se se cambiase algo."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Xosé Calvo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xosecalvo@gmail.com"

#~ msgid "VCS Project Integration"
#~ msgstr "Integración de VCS no proxecto"

#~ msgid ""
#~ "This plugin provides integration between the projects and their VCS "
#~ "infrastructure"
#~ msgstr ""
#~ "Este engadido fornece integración entre os proxectos e a súa "
#~ "infraestrutura VCS"

#~ msgid "Integrates VCS with Projects"
#~ msgstr "Integra o VCS nos proxectos"
